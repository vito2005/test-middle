const expect = require('chai').expect;
const assert = require('assert')

const { getEmployee, Employee } = require("..")
const dummyDB = {
    emptyName : {
        age: 25,
        job: {
          company: 'Открытая Школа',
          title: 'Разработчик'
        },
        expertise: [
          {
            workName: 'EdCrunch',
            timeInMonth: 12
          },
          {
            workName: 'Баду',
            timeInMonth: 1
          }
        ]
      },
      wrongName: {
        name: 8943753498,
        age: 25,
        job: {
          company: 'Открытая Школа',
          title: 'Разработчик'
        },
        expertise: [
          {
            workName: 'EdCrunch',
            timeInMonth: 12
          },
          {
            workName: 'Баду',
            timeInMonth: 1
          }
        ]
      },
      wrongAge: {
        name: 'Андрей',
        age: 'dfgs',
        job: {
          company: 'Открытая Школа',
          title: 'Разработчик'
        },
        expertise: [
          {
            workName: 'EdCrunch',
            timeInMonth: 12
          },
          {
            workName: 'Баду',
            timeInMonth: 1
          }
        ]
      },
      emptyExperiense: {
        name: 'Андрей',
        age: 18,
        job: {
          company: 'Открытая Школа',
          title: 'Разработчик'
        }
      },
      lessOneYearExperiense: {
        name: 'Андрей',
        age: 19,
        job: {
          company: 'Открытая Школа',
          title: 'Разработчик'
        },
        expertise: [
            {
              workName: 'EdCrunch',
              timeInMonth: 10
            },
            {
              workName: 'Баду',
              timeInMonth: 1
            }
          ]
      },
      moreOneYearExperiense: {
        name: 'Андрей',
        age: 20,
        job: {
          company: 'Открытая Школа',
          title: 'Разработчик'
        },
        expertise: [
            {
              workName: 'EdCrunch',
              timeInMonth: 12
            },
            {
              workName: 'Баду',
              timeInMonth: 1
            }
          ]
      },
      manyYearsExperiense: {
        name: 'Андрей',
        age: 43,
        job: {
          company: 'Открытая Школа',
          title: 'Разработчик'
        },
        expertise: [
            {
              workName: 'EdCrunch',
              timeInMonth: 12
            },
            {
              workName: 'Баду',
              timeInMonth: 120
            }
          ]
      }
}

describe('Employee main test', function () {  
    it('Should return employee description', async () => {
        let o = await getEmployee()
        const employee = new Employee(o)
        const res = employee.getInfo()
        assert.equal(res, 'Василий сейчас работает в компании Открытая Школа и родился в 1995 году. Его опыт работы более 4 лет.')
    })
})
describe('Employee class test', function () {  
    it('Should return name error', () => {
        try {
            const employee = new Employee(dummyDB.emptyName)
            const res = employee.getInfo()
        }
        catch (err) {
            assert.equal(err.message, 'Отсутствует имя сотрудника')
        }
    })
    it('Should return wrong name error', () => {
        try {
            const employee = new Employee(dummyDB.wrongName)
            const res = employee.getInfo()
        }
        catch (err) {
            assert.equal(err.message, 'Некорректное имя сотрудника')
        }
    })
    it('Should return age error', () => {
        try {
            const employee = new Employee(dummyDB.wrongAge)
            const res = employee.getInfo()
        }
        catch (err) {
            assert.equal(err.message, 'Некорректный возраст сотрудника')
        }
    })
    it('Should return empty experiense', () => {
            const employee = new Employee(dummyDB.emptyExperiense)
            const res = employee.getInfo()
            assert.equal(res, 'Андрей сейчас работает в компании Открытая Школа и родился в 2002 году. Опыт работы отсутствует.')
    })
    it('Should return less than one year experiense', () => {
            const employee = new Employee(dummyDB.lessOneYearExperiense)
            const res = employee.getInfo()
            assert.equal(res, 'Андрей сейчас работает в компании Открытая Школа и родился в 2001 году. Его опыт работы менее года.')
    })
    it('Should return more than one year experiense', () => {
            const employee = new Employee(dummyDB.moreOneYearExperiense)
            const res = employee.getInfo()
            assert.equal(res, 'Андрей сейчас работает в компании Открытая Школа и родился в 2000 году. Его опыт работы более 1 года.')
    })
    it('Should return many years experiense', () => {
            const employee = new Employee(dummyDB.manyYearsExperiense)
            const res = employee.getInfo()
            assert.equal(res, 'Андрей сейчас работает в компании Открытая Школа и родился в 1977 году. Его опыт работы более 11 лет.')
    })
})
