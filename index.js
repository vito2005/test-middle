const getEmployee = () => new Promise(resolve => {
  setTimeout(() => {
    const o = {
      name: 'Василий',
      age: 25,
      job: {
        company: 'Открытая Школа',
        title: 'Разработчик'
      },
      expertise: [
        {
          workName: 'EdCrunch',
          timeInMonth: 36
        },
        {
          workName: 'Баду',
          timeInMonth: 20
        }
      ]
    }
    resolve(o)
  }, 2000)
})

class Employee {
  constructor ({ age, name, job: { company, title }, expertise }) {
    this.age = age
    this.name = name
    this.company = company
    this.jobTitle = title
    this.expertise = expertise
  }

  set name (value) {
    if (!value) throw new Error('Отсутствует имя сотрудника')
    if (typeof value !== 'string') throw new Error('Некорректное имя сотрудника')
    this._name = value
  }

  get name () {
    return this._name
  }

  set age (value) {
    if (Number(value) < 16) throw new Error('Некорректный возраст сотрудника')
    this._age = Number(value)
  }

  get age () {
    return this._age
  }

  get yearOfBirth () {
    return 2020 - this.age
  }

  set company (value) {
    if (typeof value !== 'string') throw new Error('Некорректное название текущей компании')
    this._company = value
  }

  get company () {
    return this._company
  }

  set jobTitle (value) {
    if (typeof value !== 'string') {
      // выводим ошибку в косоль, так как title не используентся в getInfo
      console.error('Некорректная должность сотрудника')
    }
    this._jobTitle = value
  }

  get title () {
    return this._jobTitle
  }

  get totalE () {
    const totalE = Array.isArray(this.expertise) &&
      this.expertise.reduce((total, { timeInMonth }) => total + timeInMonth, 0)
    return this.formatTotal(totalE)
  }

  formatTotal (totalE) {
    if (!totalE) return 'Опыт работы отсутствует'

    const mainPhrase = 'Его опыт работы'
    totalE = ~~(totalE / 12)
    if (!totalE) {
      return `${mainPhrase} менее года`
    } else if (totalE % 10 === 1 && totalE % 100 !== 11) {
      return `${mainPhrase} более ${totalE} года`
    } else {
      return `${mainPhrase} более ${totalE} лет`
    }
  }

  getInfo () {
    return `${this.name} сейчас работает в компании ${this.company} и родился в ${this.yearOfBirth} году. ${this.totalE}.`
  }
}

(async function () {
  try {
    const o = await getEmployee()
    const employee = new Employee(o)
    console.log(employee.getInfo())
  } catch (error) {
    console.error(error)
  }
})()

module.exports = { Employee, getEmployee }
